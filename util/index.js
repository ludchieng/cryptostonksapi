export const lerp = (x0, y0, x1, y1, x) => {
  x = parseFloat(x)
  x0 = parseFloat(x0)
  x1 = parseFloat(x1)
  y0 = parseFloat(y0)
  y1 = parseFloat(y1)
  return y0 + (x-x0) * (y1-y0) / (x1-x0)
}