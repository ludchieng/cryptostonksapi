const IMA = {
  compute: (unixMilliseconds, stonkFactor) => {
    const amplitude = 0.022 * stonkFactor.volatility
    const speed = 0.0047
    const offset = 58.80
    return (
      Math.abs(
      amplitude * offset + stonkFactor.mean
      + amplitude *
      (  0.54 * Math.sin(2.5840 * speed * unixMilliseconds)
      +  0.13 * Math.sin(1.6212 * speed * unixMilliseconds)
      +  0.88 * Math.sin(2.2543 * speed * unixMilliseconds)
      +  0.51 * Math.sin(2.5467 * speed * unixMilliseconds)
      + 20.27 * Math.sin(0.0005 * speed * unixMilliseconds)
      +  2.43 * Math.sin(0.0011 * speed * unixMilliseconds)
      + 21.56 * Math.sin(0.0007 * speed * unixMilliseconds)
      +  5.43 * Math.sin(0.0012 * speed * unixMilliseconds)
      +  5.11 * Math.sin(0.0175 * speed * unixMilliseconds)
      +  2.18 * Math.sin(0.0001 * speed * unixMilliseconds)
      )
      + amplitude *
      (  5.54 * Math.sin(1.5512 * speed * unixMilliseconds)
      +  1.13 * Math.sin(0.1543 * speed * unixMilliseconds)
      +  3.88 * Math.sin(0.2125 * speed * unixMilliseconds)
      +  2.51 * Math.sin(0.3467 * speed * unixMilliseconds)
      + 19.27 * Math.sin(0.0002 * speed * unixMilliseconds)
      +  2.43 * Math.sin(0.0912 * speed * unixMilliseconds)
      + 10.56 * Math.sin(0.0011 * speed * unixMilliseconds)
      +  0.43 * Math.sin(3.0275 * speed * unixMilliseconds)
      +  0.11 * Math.sin(1.0640 * speed * unixMilliseconds)
      +  0.18 * Math.sin(2.0143 * speed * unixMilliseconds)
    )))
  }
}

const SLT = {
  compute: (unixMilliseconds, stonkFactor) => {
    const amplitude = 0.055 * stonkFactor.volatility
    const speed = 0.0056
    const offset = 105.35
    return (
      Math.abs(
      amplitude * offset + Math.pow(stonkFactor.mean * 314159, 1/2.7182818)
      + amplitude * Math.abs(Math.cos(stonkFactor.mean) * Math.pow(stonkFactor.mean, 1/2.7182818))
      + amplitude *
      (  0.05 * Math.sin(2.4840 * speed * unixMilliseconds)
      +  0.63 * Math.sin(2.0512 * speed * unixMilliseconds)
      +  2.58 * Math.sin(0.7106 * speed * unixMilliseconds)
      +  3.11 * Math.sin(0.3045 * speed * unixMilliseconds)
      + 35.75 * Math.sin(0.0012 * speed * unixMilliseconds)
      +  2.12 * Math.sin(1.0755 * speed * unixMilliseconds)
      +  1.98 * Math.sin(0.2586 * speed * unixMilliseconds)
      +  7.21 * Math.sin(0.0125 * speed * unixMilliseconds)
      + 10.92 * Math.sin(0.0101 * speed * unixMilliseconds)
      + 22.85 * Math.sin(0.0009 * speed * unixMilliseconds)
    )))
  }
}

const OGL = {
  compute: (unixMilliseconds, stonkFactor) => {
    const amplitude = 0.015 * stonkFactor.volatility
    const speed = 0.0069
    const offset = 89.35
    return (
      Math.abs(
      amplitude * offset + stonkFactor.mean / 24.3546
      + amplitude * Math.abs(Math.cos(stonkFactor.mean) * Math.pow(stonkFactor.mean, 1/2.7182818))
      + amplitude *
      (  0.75 * Math.sin(2.6840 * speed * unixMilliseconds)
      +  0.53 * Math.sin(3.0512 * speed * unixMilliseconds)
      +  0.58 * Math.sin(1.7106 * speed * unixMilliseconds)
      +  3.11 * Math.sin(0.3045 * speed * unixMilliseconds)
      + 25.75 * Math.sin(0.0102 * speed * unixMilliseconds)
      +  2.12 * Math.sin(1.0755 * speed * unixMilliseconds)
      +  1.98 * Math.sin(0.2586 * speed * unixMilliseconds)
      +  7.21 * Math.sin(0.0125 * speed * unixMilliseconds)
      + 10.92 * Math.sin(0.0091 * speed * unixMilliseconds)
      + 22.85 * Math.sin(0.0009 * speed * unixMilliseconds)
      )
      + amplitude *
      (  0.85 * Math.sin(1.1840 * speed * unixMilliseconds)
      +  0.63 * Math.sin(2.1512 * speed * unixMilliseconds)
      +  3.58 * Math.sin(0.3106 * speed * unixMilliseconds)
      +  2.11 * Math.sin(0.2045 * speed * unixMilliseconds)
      +  5.75 * Math.sin(0.1012 * speed * unixMilliseconds)
      +  3.12 * Math.sin(0.0755 * speed * unixMilliseconds)
      +  5.98 * Math.sin(0.0586 * speed * unixMilliseconds)
      +  1.21 * Math.sin(0.0215 * speed * unixMilliseconds)
      + 10.92 * Math.sin(0.0021 * speed * unixMilliseconds)
      + 17.85 * Math.sin(0.0007 * speed * unixMilliseconds)
  )))
  }
}

const ZPG = {
  compute: (unixMilliseconds, stonkFactor) => {
    const amplitude = 0.025 * stonkFactor.volatility
    const speed = 0.0019
    const offset = 41.35
    return (
      Math.abs(
      amplitude * offset + stonkFactor.mean / 5.3546
      + amplitude * Math.abs(Math.cos(stonkFactor.mean) * Math.pow(stonkFactor.mean, 1/4.7182818))
      + amplitude *
      (  0.75 * Math.sin(1.5840 * speed * unixMilliseconds)
      +  0.53 * Math.sin(2.0512 * speed * unixMilliseconds)
      +  2.58 * Math.sin(0.7106 * speed * unixMilliseconds)
      +  3.11 * Math.sin(0.3045 * speed * unixMilliseconds)
      + 24.75 * Math.sin(0.0082 * speed * unixMilliseconds)
      +  2.12 * Math.sin(1.0755 * speed * unixMilliseconds)
      +  1.98 * Math.sin(0.2586 * speed * unixMilliseconds)
      +  7.21 * Math.sin(0.0125 * speed * unixMilliseconds)
      + 30.92 * Math.sin(0.0101 * speed * unixMilliseconds)
      + 22.85 * Math.sin(0.0006 * speed * unixMilliseconds)
      )
      + amplitude *
      (  1.75 * Math.sin(1.0840 * speed * unixMilliseconds)
      +  0.53 * Math.sin(2.1512 * speed * unixMilliseconds)
      +  3.58 * Math.sin(0.3106 * speed * unixMilliseconds)
      +  2.11 * Math.sin(0.2045 * speed * unixMilliseconds)
      +  5.75 * Math.sin(0.1012 * speed * unixMilliseconds)
      +  3.12 * Math.sin(0.0755 * speed * unixMilliseconds)
      +  5.98 * Math.sin(0.0586 * speed * unixMilliseconds)
      +  1.21 * Math.sin(0.0215 * speed * unixMilliseconds)
      + 10.92 * Math.sin(0.0051 * speed * unixMilliseconds)
      +  7.85 * Math.sin(0.0019 * speed * unixMilliseconds)
  )))
  }
}

const NTM = {
  compute: (unixMilliseconds, stonkFactor) => {
    const amplitude = 0.015 * stonkFactor.volatility
    const speed = 0.0051
    const offset = 562.35
    return (
      Math.abs(
      amplitude * offset
      + amplitude * Math.abs(Math.cos(stonkFactor.mean) * Math.pow(stonkFactor.mean, 1/2.7182818))
      + amplitude *
      (  0.75 * Math.sin(2.5012 * speed * unixMilliseconds)
      +  4.53 * Math.sin(1.0840 * speed * unixMilliseconds)
      +  2.58 * Math.sin(0.7106 * speed * unixMilliseconds)
      +  3.11 * Math.sin(0.3045 * speed * unixMilliseconds)
      + 35.75 * Math.sin(0.0086 * speed * unixMilliseconds)
      +  2.12 * Math.sin(1.0755 * speed * unixMilliseconds)
      +  1.98 * Math.sin(0.0125 * speed * unixMilliseconds)
      +  7.21 * Math.sin(0.0101 * speed * unixMilliseconds)
      + 25.92 * Math.sin(0.0099 * speed * unixMilliseconds)
      + 32.85 * Math.sin(0.0002 * speed * unixMilliseconds)
      )
      + amplitude *
      (  1.75 * Math.sin(1.0840 * speed * unixMilliseconds)
      +  0.53 * Math.sin(2.3106 * speed * unixMilliseconds)
      +  1.58 * Math.sin(0.0755 * speed * unixMilliseconds)
      +  2.11 * Math.sin(0.0586 * speed * unixMilliseconds)
      +  4.75 * Math.sin(0.2045 * speed * unixMilliseconds)
      +  3.12 * Math.sin(0.1512 * speed * unixMilliseconds)
      +  2.98 * Math.sin(0.1012 * speed * unixMilliseconds)
      +  1.21 * Math.sin(0.0019 * speed * unixMilliseconds)
      +  0.92 * Math.sin(0.0215 * speed * unixMilliseconds)
      +  7.85 * Math.sin(0.0001 * speed * unixMilliseconds)
  )))
  }
}

export default { IMA, SLT, OGL, ZPG, NTM }
