import express from "express"
import dotenv from "dotenv"
import db, { dbInit, dbGetStonkFactor, dbCommitStonkfactor } from './db/index.js'
import cryptostonk from './cryptostonk/index.js'

// Init dotenv
dotenv.config()

// Create app
var app = express()

// CORS policy
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
   });

// API endpoints
app.get("/", (req, res, next) => {
    res.json({"message":"It works! Try /exchange-rates"})
});

app.get("/exchange-rates", (req, res, next) => {
    const unixMilliseconds = (new Date()).getTime()
    dbGetStonkFactor(unixMilliseconds, (stonkFactor) => {
        res.json({
            "message": {
                "IMA": cryptostonk.IMA.compute(unixMilliseconds, stonkFactor),
                "SLT": cryptostonk.SLT.compute(unixMilliseconds, stonkFactor),
                "OGL": cryptostonk.OGL.compute(unixMilliseconds, stonkFactor),
                "ZPG": cryptostonk.ZPG.compute(unixMilliseconds, stonkFactor),
                "NTM": cryptostonk.NTM.compute(unixMilliseconds, stonkFactor),
            }
        })
    })
});

app.get("/exchange-rates/:datetime", (req, res, next) => {
    const unixMilliseconds = Date.parse(req.params.datetime)
    if (isNaN(unixMilliseconds)) {
        res.json({
            "message": "Error: Could not parse datetime, example: 2020-11-24T17:26:58Z"
        })
    } else {
        dbGetStonkFactor(unixMilliseconds, (stonkFactor) => {
            res.json({
                "message": {
                    "stonkimac": cryptostonk.stonkimac.compute(unixMilliseconds, stonkFactor),
                    "saltcoin": cryptostonk.saltcoin.compute(unixMilliseconds, stonkFactor),
                }
            })
        })
    }
});

app.get("/db/init", (req, res, next) => {
    if (req.query.password != process.env.PASSWORD) {
        res.status(403).json({"message": "Forbiden :("})
        return;
    }

    dbInit()
    res.json({"message": "Database initialized"})
});

app.get("/db/commit/stonkfactor", (req, res, next) => {
    if (req.query.password != process.env.PASSWORD) {
        res.status(403).json({"message": "Forbiden :("})
        return;
    }
    if (!req.query.timestamp || !req.query.mean || !req.query.volatility) {
        res.status(422).json({"message": "Missing params: expected 'timestamp', 'mean' and 'volatility'"})
        return;
    }
    const now = Date.now()
    const timestamp = Date.parse(req.query.timestamp)
    const mean = parseFloat(req.query.mean)
    const volatility = parseFloat(req.query.volatility)

    if (isNaN(timestamp)) {
        res.status(422).json({"message": "Invalid param: failed to parse 'timestamp' as a datetime value"})
        return;
    }
    if (timestamp < now) {
        res.status(422).json({"message": "Invalid param: 'timestamp' cannot refer to past time. It must be UTC+0."})
        return;
    }
    if (isNaN(mean) || isNaN(volatility)) {
        res.status(422).json({"message": "Invalid params: expected 'mean' and 'volatility' to be numeric"})
        return;
    }
    dbCommitStonkfactor(now, timestamp, mean, volatility);
    res.json({"message": 'Done'})
});

// Fallback endpoint
app.use(function(req, res){
    res.status(404).json({"message":"Not found :( Try /exchange-rates"});
});

// Run server
app.listen(8000);
