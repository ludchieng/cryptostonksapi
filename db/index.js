import sqlite3 from 'sqlite3'
import { lerp } from '../util/index.js'

const db = new sqlite3.Database("db.sqlite", (err) => {
  if (err)
    console.error(err.message)
});

export const dbInit = () => {
  db.run(`
    CREATE TABLE "stonkfactor" (
      "timestamp"	 TEXT    NOT NULL,
      "mean"	     INTEGER NOT NULL,
      "volatility" INTEGER NOT NULL,
      PRIMARY KEY("timestamp")
    );
  `)
}

export const dbCommitStonkfactor = (now, timestamp, mean, volatility) => {
  const formatedNow = new Date(now).toISOString().replace("T"," ").replace("Z","")
  const formatedTimestamp = new Date(timestamp).toISOString().replace("T"," ").replace("Z","")
  db.run(
    `INSERT INTO stonkfactor VALUES (
      ?, (
        SELECT mean FROM stonkfactor
        ORDER BY timestamp DESC
        LIMIT 1
      ),(
        SELECT volatility FROM stonkfactor
        ORDER BY timestamp DESC
        LIMIT 1
    ))`,
    [ formatedNow ]
  )
  db.run(
    "INSERT INTO stonkfactor VALUES (?, ?, ?);",
    [ formatedTimestamp, mean, volatility ]
  )
}

export const dbGetStonkFactor = (datetime, callback) => {
  datetime = new Date(datetime).toISOString().replace("T"," ").replace("Z","")
  const sqlGetSurroundingTimestamps = `
    SELECT
      strftime('%s',$datetime,'utc') AS datetime,
      strftime('%s',sf.timestamp,'utc') AS timestamp,
      sf.mean AS mean,
      sf.volatility AS volatility
    FROM stonkfactor AS sf, (
      SELECT timestamp
      FROM stonkfactor
      WHERE strftime('%s',timestamp,'utc') < strftime('%s',$datetime,'utc')
      ORDER BY timestamp DESC
      LIMIT 1
    ) AS prevDate
    WHERE sf.timestamp >= prevDate.timestamp
    LIMIT 2;
  `

  db.all(sqlGetSurroundingTimestamps, { $datetime: datetime }, (err, rows) => {
    if (rows === undefined || rows.length < 1) {
      console.error("Error querying db stonkfactor: no datetime around the given datetime")
      return;
    }
    const prev = rows[0]
    const next = (rows.length === 2) ? rows[1] : null
    const mean = (next !== null)       ? lerp(prev.timestamp, prev.mean, next.timestamp, next.mean, prev.datetime) : prev.mean
    const volatility = (next !== null) ? lerp(prev.timestamp, prev.volatility, next.timestamp, next.volatility, prev.datetime) : prev.volatility
    callback({mean, volatility})
  })
}
/*
export const dbGetStonkFactorAll = (callback) => {
  const sql = `
    SELECT strftime('%s',timestamp,'utc') as unixts, timestamp, mean, volatility
    FROM stonkfactor;
  `

  db.all(sql, (err, rows) => {
     callback(rows)
  })
}
*/
export default db
