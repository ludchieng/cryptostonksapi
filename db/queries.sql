
-- Get prev date from now
SELECT timestamp
FROM stonkfactor
WHERE CAST(strftime('%s',timestamp,'utc') as INTEGER) < CAST(strftime('%s','now','utc') as INTEGER)
ORDER BY timestamp DESC
LIMIT 1;

-- Get prev date and next around now
SELECT sf.timestamp, sf.mean, sf.volatility
FROM stonkfactor AS sf, (
	SELECT timestamp
	FROM stonkfactor
	WHERE CAST(strftime('%s',timestamp,'utc') as INTEGER) < CAST(strftime('%s','now','utc') as INTEGER)
	ORDER BY timestamp DESC
	LIMIT 1
) AS prevDate
WHERE sf.timestamp >= prevDate.timestamp
LIMIT 2
